package com.example.myapplication.common

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import app.cash.molecule.AndroidUiDispatcher.Companion.Main
import app.cash.molecule.launchMolecule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.shareIn
import kotlin.coroutines.CoroutineContext

const val CHANNEL_CAPACITY = 50
abstract class BaseViewModel<UiModel : Any, UiEvent : Any>(
    moleculeMain: CoroutineContext
) : ViewModel() {

    protected val scope = CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate + moleculeMain)

    private val eventsChannel = Channel<UiEvent>(CHANNEL_CAPACITY)

    private val events = eventsChannel
        .consumeAsFlow()
        .shareIn(scope, SharingStarted.Lazily)

    val models by lazy {
        scope.launchMolecule {
            models(events = events)
        }
    }

    fun sendEvent(uiEvent: UiEvent) {
        val result = eventsChannel.trySend(uiEvent)
        if (!result.isSuccess && !result.isClosed) {
            error("Event buffer overflow")
        }
    }

    override fun onCleared() {
        super.onCleared()
        scope.cancel()
    }

    @Composable
    abstract fun models(events: Flow<UiEvent>): UiModel
}
