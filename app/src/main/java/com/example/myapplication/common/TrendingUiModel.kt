package com.example.myapplication.common

import androidx.paging.PagingData
import com.example.myapplication.trending.TrendingItem
import com.example.myapplication.trending.paging.TrendingLoadingStatus
import kotlinx.coroutines.flow.Flow

data class TrendingUiModel(
    val list: Flow<PagingData<TrendingItem>>,
    val initialLoadingStatus: TrendingLoadingStatus.InitialLoadingStatus,
    val paginatedLoadingStatus: TrendingLoadingStatus.PaginatedLoadingStatus,
)
