package com.example.myapplication.common

sealed class UiState<out Model : Any> {
    object Uninitialized : UiState<Nothing>()
    object Loading : UiState<Nothing>()
    object Error : UiState<Nothing>()
    data class Success<out Model : Any>(val item: Model) : UiState<Model>()
}
