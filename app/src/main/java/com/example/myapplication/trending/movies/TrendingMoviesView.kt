package com.example.myapplication.trending.movies

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.myapplication.destinations.MovieDetailsViewDestination
import com.example.myapplication.detail.DetailsScreenArgs
import com.example.myapplication.trending.TrendingListView
import com.example.myapplication.trending.TrendingListViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@RootNavGraph(start = true)
@Destination
@Composable
fun TrendingMovies(
    paddingValues: PaddingValues,
    navigator: DestinationsNavigator,
    viewModel: TrendingListViewModel = hiltViewModel<TrendingMoviesViewModel>()

) {
    TrendingListView(viewModel, paddingValues, navigator) { id ->
        MovieDetailsViewDestination(
            DetailsScreenArgs(id)
        )
    }
}
