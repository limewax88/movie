package com.example.myapplication.trending.paging

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.myapplication.trending.TrendingItem
import kotlinx.coroutines.channels.Channel
import javax.inject.Inject

private const val PAGE_SIZE = 10
interface TrendingPagerFactory {
    val loadingChannel: Channel<TrendingLoadingStatus>
    fun create(loadData: suspend (Int) -> PagingResult): Pager<Int, TrendingItem>
}

class RealTrendingPagerFactory @Inject constructor() : TrendingPagerFactory {

    override val loadingChannel = Channel<TrendingLoadingStatus>(capacity = Channel.CONFLATED)

    override fun create(loadData: suspend (Int) -> PagingResult): Pager<Int, TrendingItem> {
        return Pager(PagingConfig(PAGE_SIZE)) {
            TrendingPageDataSource(loadingChannel, loadData)
        }
    }
}
