package com.example.myapplication.trending.movies

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TrendingMovie(
    @SerialName("id") val id: Long,
    @SerialName("overview") val overview: String,
    @SerialName("poster_path") val posterPath: String?,
    @SerialName("title") val title: String,
    @SerialName("vote_average") val voteAverage: Float,
)
