package com.example.myapplication.trending

import com.example.myapplication.ImageSizes
import com.example.myapplication.trending.movies.TrendingMovie
import com.example.myapplication.trending.paging.PagingResult
import com.example.myapplication.trending.shows.TrendingShow
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface TrendingRepository {
    suspend fun showsSortedByRating(page: Int): PagingResult
    suspend fun moviesSortedByRating(page: Int): PagingResult
}
class RealTrendingRepository @Inject constructor(
    private val trendingService: TrendingService
): TrendingRepository {

    @Throws(Exception::class)
    override suspend fun showsSortedByRating(page: Int): PagingResult {
        return withContext(Dispatchers.IO) {
//            delay(3000L) // artificial 3 second delay
            val response = trendingService.trendingShows(page)
            val items = response.results
                .sortedByDescending(TrendingShow::voteAverage)
                .map(TrendingShow::toTrendingItem)

            PagingResult(
                items,
                response.page
            )
        }
    }

    @Throws(Exception::class)
    override suspend fun moviesSortedByRating(page: Int): PagingResult {
        return withContext(Dispatchers.IO) {
//            delay(3000L) // artificial 3 second delay
            val response = trendingService.trendingMovies(page)
            val items = response.results
                .sortedByDescending(TrendingMovie::voteAverage)
                .map(TrendingMovie::toTrendingItem)
            PagingResult(
                items,
                response.page
            )
        }
    }
}

private fun TrendingShow.toTrendingItem() = TrendingItem(
    id = id,
    imageUrl = "${ImageSizes.baseImageUrl}w92$posterPath",
    description = overview,
    title = name
)

private fun TrendingMovie.toTrendingItem() = TrendingItem(
    id = id,
    imageUrl = "${ImageSizes.baseImageUrl}w92$posterPath",
    description = overview,
    title = title
)
