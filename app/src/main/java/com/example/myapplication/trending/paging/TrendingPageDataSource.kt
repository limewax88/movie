package com.example.myapplication.trending.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.myapplication.trending.TrendingItem
import kotlinx.coroutines.channels.Channel

data class PagingResult(
    val list: List<TrendingItem>,
    val page: Int
)

sealed class TrendingLoadingStatus {
    sealed class InitialLoadingStatus : TrendingLoadingStatus() {
        object Loading : InitialLoadingStatus()
        object Success : InitialLoadingStatus()
        data class Failure(val message: String) : InitialLoadingStatus()
    }
    sealed class PaginatedLoadingStatus : TrendingLoadingStatus() {
        object Loading : PaginatedLoadingStatus()
        object Idle : PaginatedLoadingStatus()
        data class Failure(val message: String) : PaginatedLoadingStatus()
    }
}

class TrendingPageDataSource constructor(
    private val loadingStatus: Channel<TrendingLoadingStatus>,
    private val loadData: suspend (Int) -> PagingResult
) : PagingSource<Int, TrendingItem>() {

    // Not supporting refresh
    override fun getRefreshKey(state: PagingState<Int, TrendingItem>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TrendingItem> {
        params.updateStatusLoading()
        return try {
            val result = loadData(params.key ?: 1)
            params.updateStatusSuccess()
            LoadResult.Page(
                data = result.list,
                prevKey = null,
                nextKey = result.page + 1
            )
        } catch (e: Exception) {
            params.updateStatusError(e.message ?: "An error occured")
            LoadResult.Error(Exception(e))
        }
    }

    private fun LoadParams<Int>.updateStatusLoading() {
        if (key == null) {
            loadingStatus.trySend(TrendingLoadingStatus.InitialLoadingStatus.Loading).getOrThrow()
        } else {
            loadingStatus.trySend(TrendingLoadingStatus.PaginatedLoadingStatus.Loading).getOrThrow()
        }
    }

    private fun LoadParams<Int>.updateStatusError(errorMessage: String) {
        if (key == null) {
            loadingStatus.trySend(TrendingLoadingStatus.InitialLoadingStatus.Failure(errorMessage)).getOrThrow()
        } else {
            loadingStatus.trySend(TrendingLoadingStatus.PaginatedLoadingStatus.Failure(errorMessage)).getOrThrow()
        }
    }

    private fun LoadParams<Int>.updateStatusSuccess() {
        if (key == null) {
            loadingStatus.trySend(TrendingLoadingStatus.InitialLoadingStatus.Success).getOrThrow()
        } else {
            loadingStatus.trySend(TrendingLoadingStatus.PaginatedLoadingStatus.Idle).getOrThrow()
        }
    }
}
