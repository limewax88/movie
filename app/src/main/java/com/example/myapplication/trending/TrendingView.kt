package com.example.myapplication.trending

import android.widget.Toast
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.myapplication.Failure
import com.example.myapplication.R
import com.example.myapplication.common.Progress
import com.example.myapplication.common.ProgressRow
import com.example.myapplication.common.TrendingUiModel
import com.example.myapplication.trending.paging.TrendingLoadingStatus
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.spec.Direction

@Composable
fun TrendingListView(
    viewModel: TrendingListViewModel,
    paddingValues: PaddingValues,
    navigator: DestinationsNavigator,
    navigateTo: (Long) -> Direction
) {
    val model = remember { viewModel.models }.collectAsState().value
    when (model.initialLoadingStatus) {
        is TrendingLoadingStatus.InitialLoadingStatus.Failure -> {
            Failure(
                R.string.movie_list_error_message,
                paddingValues
            ) {
                viewModel.sendEvent(TrendingListEvents.TryAgain)
            }
        }
        TrendingLoadingStatus.InitialLoadingStatus.Loading -> Progress()
        TrendingLoadingStatus.InitialLoadingStatus.Success -> {
            // do nothing
        }
    }

    TrendingList(model = model, paddingValues) { id ->
        navigator.navigate(navigateTo(id))
    }

    if (model.paginatedLoadingStatus is TrendingLoadingStatus.PaginatedLoadingStatus.Failure) {
        Toast.makeText(
            LocalContext.current,
            model.paginatedLoadingStatus.message,
            Toast.LENGTH_LONG
        ).show()
    }
}

@Composable
fun TrendingList(
    model: TrendingUiModel,
    paddingValues: PaddingValues,
    onClick: (Long) -> Unit
) {
    val items = model.list.collectAsLazyPagingItems()
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(4.dp),
        contentPadding = paddingValues
    ) {
        items(items) { trending ->
            TrendingItemView(trending!!) {
                onClick(trending.id)
            }
        }
        if (model.paginatedLoadingStatus is TrendingLoadingStatus.PaginatedLoadingStatus.Loading) {
            item {
                ProgressRow()
            }
        }
    }
}

@Composable
fun TrendingItemView(trendingItem: TrendingItem, onClick: () -> Unit) {
    val isExpanded = remember { mutableStateOf(false) }
    Card(
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth()
            .clickable { onClick() },
        border = BorderStroke(1.dp, Color.LightGray),
        elevation = 2.dp
    ) {
        Row(
            Modifier.padding(8.dp),
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(trendingItem.imageUrl)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(R.drawable.ic_baseline_image_24),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.clip(CircleShape)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Column(
                verticalArrangement = Arrangement.Center
            ) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    buildAnnotatedString {
                        withStyle(style = MaterialTheme.typography.h6.toSpanStyle()) {
                            append(trendingItem.title)
                        }
                    }
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .animateContentSize()
                        .clickable { isExpanded.value = !isExpanded.value },
                    text = trendingItem.description,
                    style = MaterialTheme.typography.body2,
                    maxLines = if (isExpanded.value) Int.MAX_VALUE else 1,
                    overflow = if (isExpanded.value) TextOverflow.Clip else TextOverflow.Ellipsis
                )
            }
        }
    }
}
