package com.example.myapplication.trending.shows

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.myapplication.destinations.TvDetailsViewDestination
import com.example.myapplication.detail.DetailsScreenArgs
import com.example.myapplication.trending.TrendingListView
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@RootNavGraph
@Destination
@Composable
fun TrendingShows(
    paddingValues: PaddingValues,
    navigator: DestinationsNavigator,
    viewModel: TrendingShowsViewModel = hiltViewModel()
) {

    TrendingListView(viewModel, paddingValues, navigator) { id ->
        TvDetailsViewDestination(
            DetailsScreenArgs(id)
        )
    }
}
