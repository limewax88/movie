package com.example.myapplication.trending

data class TrendingItem(
    val id: Long,
    val imageUrl: String,
    val title: String,
    val description: String,
)
