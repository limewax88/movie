package com.example.myapplication.trending

import com.example.myapplication.trending.movies.TrendingMovie
import com.example.myapplication.trending.shows.TrendingShow
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

private interface Original {
    @GET("trending/{media_type}/{time_window}")
    suspend fun getTrendingMovies(
        @Path("media_type") type: String,
        @Path("time_window") timeWindow: String,
        @Query("page") page: Int
    ): TrendingResults<TrendingMovie>

    @GET("trending/{media_type}/{time_window}")
    suspend fun getTrendingShows(
        @Path("media_type") type: String,
        @Path("time_window") timeWindow: String,
        @Query("page") page: Int
    ): TrendingResults<TrendingShow>
}

interface TrendingService {

    suspend fun trendingMovies(page: Int): TrendingResults<TrendingMovie>

    suspend fun trendingShows(page: Int): TrendingResults<TrendingShow>
}

class TrendingApi @Inject constructor(
    retrofit: Retrofit
) : TrendingService {

    private val original = retrofit.create(Original::class.java)

    override suspend fun trendingMovies(page: Int): TrendingResults<TrendingMovie> {
        return original.getTrendingMovies("movie", "week", page)
    }

    override suspend fun trendingShows(page: Int): TrendingResults<TrendingShow> {
        return original.getTrendingShows("tv", "week", page)
    }
}
