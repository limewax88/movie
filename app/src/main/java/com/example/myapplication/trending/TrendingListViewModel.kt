package com.example.myapplication.trending

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.myapplication.common.BaseViewModel
import com.example.myapplication.common.CollectEffect
import com.example.myapplication.common.TrendingUiModel
import com.example.myapplication.trending.TrendingListEvents.TryAgain
import com.example.myapplication.trending.paging.PagingResult
import com.example.myapplication.trending.paging.TrendingLoadingStatus.InitialLoadingStatus
import com.example.myapplication.trending.paging.TrendingLoadingStatus.PaginatedLoadingStatus
import com.example.myapplication.trending.paging.TrendingPagerFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlin.coroutines.CoroutineContext

abstract class TrendingListViewModel(
    moleculeMain: CoroutineContext,
    private val trendingPagerFactory: TrendingPagerFactory
) : BaseViewModel<TrendingUiModel, TrendingListEvents>(moleculeMain) {

    private val loadingStatusUpdates = trendingPagerFactory.loadingChannel.receiveAsFlow()

    @Composable
    override fun models(events: Flow<TrendingListEvents>): TrendingUiModel {

        var initialLoadingStatus: InitialLoadingStatus by remember {
            mutableStateOf(InitialLoadingStatus.Loading)
        }

        var paginatedLoadingStatus: PaginatedLoadingStatus by remember {
            mutableStateOf(PaginatedLoadingStatus.Idle)
        }

        var pagingDataFlow: Flow<PagingData<TrendingItem>> by remember {
            mutableStateOf(createPagerFlow(scope))
        }

        CollectEffect(loadingStatusUpdates) { loadingStatus ->
            when (loadingStatus) {
                is InitialLoadingStatus -> {
                    initialLoadingStatus = loadingStatus
                }
                is PaginatedLoadingStatus -> {
                    paginatedLoadingStatus = loadingStatus
                }
            }
        }

        CollectEffect(events) { event ->
            when (event) {
                TryAgain -> {
                    pagingDataFlow = createPagerFlow(scope)
                }
            }
        }

        return TrendingUiModel(
            list = pagingDataFlow,
            initialLoadingStatus = initialLoadingStatus,
            paginatedLoadingStatus = paginatedLoadingStatus
        )
    }

    private fun createPagerFlow(scope: CoroutineScope) = trendingPagerFactory.create { page ->
        loadData(page)
    }.flow.cachedIn(scope)

    abstract suspend fun loadData(page: Int): PagingResult
}
