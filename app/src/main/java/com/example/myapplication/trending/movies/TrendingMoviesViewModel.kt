package com.example.myapplication.trending.movies

import com.example.myapplication.di.MoleculeMain
import com.example.myapplication.trending.TrendingListViewModel
import com.example.myapplication.trending.TrendingRepository
import com.example.myapplication.trending.paging.PagingResult
import com.example.myapplication.trending.paging.TrendingPagerFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@HiltViewModel
class TrendingMoviesViewModel @Inject constructor(
    private val trendingRepository: TrendingRepository,
    @MoleculeMain val moleculeMain: CoroutineContext,
    trendingPagerFactory: TrendingPagerFactory,
) : TrendingListViewModel(moleculeMain, trendingPagerFactory) {

    override suspend fun loadData(page: Int): PagingResult {
        return trendingRepository.moviesSortedByRating(page)
    }
}
