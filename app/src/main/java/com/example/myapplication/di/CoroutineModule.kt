package com.example.myapplication.di

import app.cash.molecule.AndroidUiDispatcher
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Qualifier
import kotlin.coroutines.CoroutineContext

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Io


@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MoleculeMain

@Module
@InstallIn(SingletonComponent::class)
class CoroutineModule {

    @Io
    @Provides
    fun providesIoContext(): CoroutineContext {
        return Dispatchers.IO
    }

    @MoleculeMain
    @Provides
    fun providesMoleculeMainContext(): CoroutineContext {
        return AndroidUiDispatcher.Main
    }
}