package com.example.myapplication.di

import com.example.myapplication.BuildConfig
import com.example.myapplication.detail.DetailRepository
import com.example.myapplication.detail.DetailService
import com.example.myapplication.detail.DetailsApi
import com.example.myapplication.detail.RealDetailRepository
import com.example.myapplication.network.AuthInterceptor
import com.example.myapplication.trending.RealTrendingRepository
import com.example.myapplication.trending.TrendingApi
import com.example.myapplication.trending.TrendingRepository
import com.example.myapplication.trending.TrendingService
import com.example.myapplication.trending.paging.RealTrendingPagerFactory
import com.example.myapplication.trending.paging.TrendingPagerFactory
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
private annotation class BaseUrl

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
private annotation class JsonConverter

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkModule {

    @Binds
    abstract fun bindsTrendingService(trendingApi: TrendingApi): TrendingService

    @Binds
    abstract fun bindsDetailsService(detailsApi: DetailsApi): DetailService

    @Binds
    abstract fun bindsTrendingRepository(realTrendingRepository: RealTrendingRepository): TrendingRepository

    @Binds
    abstract fun bindsDetailRepository(detailRepository: RealDetailRepository): DetailRepository

    @Binds
    abstract fun bindsTrendingPagerFactory(trendingPagerFactory: RealTrendingPagerFactory): TrendingPagerFactory

    companion object {
        @Provides
        @BaseUrl
        fun providesBaseUrl(): String = BuildConfig.SERVER_ENDPOINT

        @Provides
        @JsonConverter
        fun providesJsonConverter(): Converter.Factory = Json {
            ignoreUnknownKeys = true
        }.asConverterFactory("application/json".toMediaType())

        @Provides
        @Singleton
        fun providesOkHttpClient(authInterceptor: AuthInterceptor): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .build()
        }

        @Singleton
        @Provides
        fun provideRetrofit(
            @BaseUrl baseUrl: String,
            @JsonConverter converter: Converter.Factory,
            okHttpClient: OkHttpClient,
        ): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .client(okHttpClient)
                .build()
        }
    }
}
