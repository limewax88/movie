package com.example.myapplication

import com.example.myapplication.destinations.TrendingMoviesDestination
import com.example.myapplication.destinations.TrendingShowsDestination
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec

enum class BottomBarDestination(
    val direction: DirectionDestinationSpec,
    val icon: Int,
    val label: Int
) {
    Movies(TrendingMoviesDestination, R.drawable.ic_baseline_local_movies_24, R.string.movies),
    TvShows(TrendingShowsDestination, R.drawable.ic_baseline_tv_24, R.string.television),
}
