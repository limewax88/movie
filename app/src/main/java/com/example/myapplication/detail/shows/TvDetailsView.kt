package com.example.myapplication.detail.shows

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.myapplication.Failure
import com.example.myapplication.R
import com.example.myapplication.common.Progress
import com.example.myapplication.common.UiState
import com.example.myapplication.detail.DetailsEvents
import com.example.myapplication.detail.DetailsScreenArgs
import com.example.myapplication.detail.DetailsView
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph

@RootNavGraph
@Composable
@Destination(navArgsDelegate = DetailsScreenArgs::class)
fun TvDetailsView(
    viewModel: TvDetailsViewModel = hiltViewModel()
) {
    when (val models = remember { viewModel.models }.collectAsState().value) {
        UiState.Error -> Failure(message = R.string.show_detail_error_message, paddingValues = PaddingValues(0.dp)) {
            viewModel.sendEvent(DetailsEvents.LoadDetails)
        }
        UiState.Loading -> Progress()
        is UiState.Success -> DetailsView(detailsItem = models.item)
        UiState.Uninitialized -> viewModel.sendEvent(DetailsEvents.LoadDetails)
    }
}
