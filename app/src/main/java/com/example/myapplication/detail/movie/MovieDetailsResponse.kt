package com.example.myapplication.detail.movie

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MovieDetailsResponse(
    @SerialName("id") val id: Long,
    @SerialName("poster_path") val poster_path: String,
    @SerialName("backdrop_path") val backdrop_path: String,
    @SerialName("original_title") val original_title: String,
    @SerialName("overview") val overview: String,
    @SerialName("popularity") val popularity: Float,
    @SerialName("release_date") val release_date: String,
    @SerialName("revenue") val revenue: Long,
    @SerialName("runtime") val runtime: Long,
    @SerialName("title") val title: String,
    @SerialName("tagline") val tagline: String,
    @SerialName("vote_average") val vote_average: Float,
    @SerialName("vote_count") val vote_count: Long,
)
