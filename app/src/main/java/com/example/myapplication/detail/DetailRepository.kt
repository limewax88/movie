package com.example.myapplication.detail

import com.example.myapplication.ImageSizes
import com.example.myapplication.detail.movie.MovieDetailsResponse
import com.example.myapplication.detail.shows.TvDetailsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface DetailRepository {
    suspend fun movieById(id: Long): DetailsItem
    suspend fun showById(id: Long): DetailsItem
}

class RealDetailRepository @Inject constructor(
    private val detailService: DetailService
) : DetailRepository {

    @Throws(Exception::class)
    override suspend fun movieById(id: Long): DetailsItem {
        return withContext(Dispatchers.IO) {
            detailService.getMovieById(id)
                .toMovieDetails()
        }
    }

    override suspend fun showById(id: Long): DetailsItem {
        return withContext(Dispatchers.IO) {
            detailService.geTvById(id)
                .toTvDetails()
        }
    }
}

private fun TvDetailsResponse.toTvDetails() = DetailsItem(
    imageUrl = "${ImageSizes.baseImageUrl}w1280$backdrop_path",
    description = overview,
    title = original_name,
    releaseDate = first_air_date,
    runtime = episode_run_time.average().toLong(),
    rating = vote_average
)
private fun MovieDetailsResponse.toMovieDetails() = DetailsItem(
    imageUrl = "${ImageSizes.baseImageUrl}w1280$backdrop_path",
    description = overview,
    title = original_title,
    releaseDate = release_date,
    runtime = runtime,
    rating = vote_average
)
