package com.example.myapplication.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.intl.LocaleList
import androidx.compose.ui.text.toUpperCase
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.myapplication.R
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.gowtham.ratingbar.StepSize

data class DetailsScreenArgs(
    val id: Long,
)
@Composable
fun DetailsView(detailsItem: DetailsItem) {
    Column(
        Modifier
            .verticalScroll(rememberScrollState())
            .fillMaxSize()
    ) {
        Box(
            modifier = Modifier.fillMaxWidth()
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(detailsItem.imageUrl)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(R.drawable.ic_baseline_image_24),
                contentDescription = null,
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .fillMaxSize()
            )
            Text(
                style = MaterialTheme.typography.h4,
                text = detailsItem.title.toUpperCase(LocaleList.current),
                color = Color.White,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.Black.copy(alpha = 0.5f))
                    .align(Alignment.BottomStart)
                    .padding(8.dp)
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            modifier = Modifier.padding(8.dp),
            text = buildAnnotatedString {
                withStyle(style = MaterialTheme.typography.h6.toSpanStyle()) {
                    append(stringResource(id = R.string.release_date))
                    append(" ")
                    append(detailsItem.releaseDate)
                }
            }
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            modifier = Modifier.padding(8.dp),
            text = buildAnnotatedString {
                withStyle(style = MaterialTheme.typography.h6.toSpanStyle()) {
                    append(stringResource(id = R.string.running_time))
                    append(" ${detailsItem.runtime} ")
                    append(stringResource(id = R.string.minutes))
                }
            }
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            modifier = Modifier.padding(8.dp),
            style = MaterialTheme.typography.h6,
            fontWeight = FontWeight.Bold,
            text = stringResource(id = R.string.overview)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            modifier = Modifier.padding(8.dp),
            style = MaterialTheme.typography.body1,
            text = detailsItem.description
        )
        Spacer(modifier = Modifier.height(24.dp))
        RatingBar(
            modifier = Modifier.align(Alignment.CenterHorizontally),
            value = detailsItem.rating,
            config = RatingBarConfig()
                .stepSize(StepSize.HALF)
                .numStars(10)
                .style(RatingBarStyle.HighLighted),
            onValueChange = {},
            onRatingChanged = {}
        )
        Spacer(modifier = Modifier.height(24.dp))
    }
}
