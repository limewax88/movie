package com.example.myapplication.detail

import com.example.myapplication.detail.movie.MovieDetailsResponse
import com.example.myapplication.detail.shows.TvDetailsResponse
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import javax.inject.Inject

private interface Original {

    @GET("movie/{movie_id}")
    suspend fun getMovieById(@Path("movie_id") id: Long): MovieDetailsResponse

    @GET("tv/{tv_id}")
    suspend fun geTvById(@Path("tv_id") id: Long): TvDetailsResponse
}

interface DetailService {

    suspend fun getMovieById(id: Long): MovieDetailsResponse

    suspend fun geTvById(id: Long): TvDetailsResponse
}

class DetailsApi @Inject constructor(
    retrofit: Retrofit
) : DetailService, Original by retrofit.create(Original::class.java)
