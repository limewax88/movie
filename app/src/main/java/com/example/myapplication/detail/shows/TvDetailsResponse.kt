package com.example.myapplication.detail.shows

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TvDetailsResponse(
    @SerialName("id") val id: Long,
    @SerialName("backdrop_path") val backdrop_path: String,
    @SerialName("episode_run_time") val episode_run_time: List<Long>,
    @SerialName("first_air_date") val first_air_date: String,
    @SerialName("original_name") val original_name: String,
    @SerialName("overview") val overview: String,
    @SerialName("popularity") val popularity: Float,
    @SerialName("poster_path") val poster_path: String,
    @SerialName("tagline") val tagline: String,
    @SerialName("vote_average") val vote_average: Float,
    @SerialName("vote_count") val vote_count: Long,
)
