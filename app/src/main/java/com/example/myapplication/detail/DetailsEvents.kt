package com.example.myapplication.detail

sealed class DetailsEvents {
    object LoadDetails : DetailsEvents()
}
