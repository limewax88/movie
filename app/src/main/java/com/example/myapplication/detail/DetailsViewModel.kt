package com.example.myapplication.detail

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import com.example.myapplication.common.BaseViewModel
import com.example.myapplication.common.CollectEffect
import com.example.myapplication.common.UiState
import com.example.myapplication.navArgs
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

abstract class DetailsViewModel constructor(
    savedStateHandle: SavedStateHandle,
    moleculeMain: CoroutineContext
) : BaseViewModel<UiState<DetailsItem>, DetailsEvents>(moleculeMain) {

    private val id = savedStateHandle.navArgs<DetailsScreenArgs>().id

    @Composable
    override fun models(events: Flow<DetailsEvents>): UiState<DetailsItem> {
        var details by remember { mutableStateOf<UiState<DetailsItem>>(UiState.Uninitialized) }
        CollectEffect(events) { event ->
            when (event) {
                DetailsEvents.LoadDetails -> {
                    launch {
                        details = UiState.Loading
                        details = getDetailsById(id)
                    }
                }
            }
        }
        return details
    }

    abstract suspend fun getData(id: Long): DetailsItem

    private suspend fun getDetailsById(id: Long): UiState<DetailsItem> {
        return try {
            UiState.Success(getData(id))
        } catch (e: Exception) {
            Log.e(DetailsViewModel::class.simpleName, "$e")
            UiState.Error
        }
    }
}
