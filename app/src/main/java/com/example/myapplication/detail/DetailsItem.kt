package com.example.myapplication.detail

data class DetailsItem(
    val imageUrl: String,
    val title: String,
    val description: String,
    val releaseDate: String,
    val runtime: Long,
    val rating: Float
)
