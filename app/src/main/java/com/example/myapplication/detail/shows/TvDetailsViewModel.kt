package com.example.myapplication.detail.shows

import androidx.lifecycle.SavedStateHandle
import com.example.myapplication.detail.DetailRepository
import com.example.myapplication.detail.DetailsItem
import com.example.myapplication.detail.DetailsViewModel
import com.example.myapplication.di.MoleculeMain
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@HiltViewModel
class TvDetailsViewModel @Inject constructor(
    private val detailRepository: DetailRepository,
    savedStateHandle: SavedStateHandle,
    @MoleculeMain val moleculeMain: CoroutineContext
) : DetailsViewModel(savedStateHandle, moleculeMain) {
    override suspend fun getData(id: Long): DetailsItem {
        return detailRepository.showById(id)
    }
}
