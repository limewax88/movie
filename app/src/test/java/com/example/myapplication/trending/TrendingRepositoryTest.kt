package com.example.myapplication.trending

import com.example.myapplication.ImageSizes
import com.example.myapplication.trending.movies.TrendingMovie
import com.example.myapplication.trending.paging.PagingResult
import com.example.myapplication.trending.shows.TrendingShow
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Test
import java.util.concurrent.TimeoutException
import kotlin.test.assertFailsWith

class TrendingRepositoryTest {

    @Test
    fun `moviesSortedByRating returns PagingResult`() = runTest {
        val repository = RealTrendingRepository(FakeTrendingService())
        val expected = PagingResult(
            list = listOf(
                TrendingItem(
                    id = 2L,
                    imageUrl = "${ImageSizes.baseImageUrl}w92/DoctorStrange",
                    title = "DoctorStrange",
                    description = "DoctorStrange"
                ),
                TrendingItem(
                    id = 1L,
                    imageUrl = "${ImageSizes.baseImageUrl}w92/Batman",
                    title = "Batman",
                    description = "Batman"
                ),
            ),
            page = 1
        )
        assertThat(repository.moviesSortedByRating(1)).isEqualTo(expected)
    }

    @Test
    fun `showsSortedByRating returns PagingResult`() = runTest {
        val repository = RealTrendingRepository(FakeTrendingService())
        val expected = PagingResult(
            list = listOf(
                TrendingItem(
                    id = 2L,
                    imageUrl = "${ImageSizes.baseImageUrl}w92/OnePiece",
                    title = "OnePiece",
                    description = "OnePiece"
                ),
                TrendingItem(
                    id = 1L,
                    imageUrl = "${ImageSizes.baseImageUrl}w92/DragonBall",
                    title = "DragonBall",
                    description = "DragonBall"
                ),
            ),
            page = 1
        )
        assertThat(repository.showsSortedByRating(1)).isEqualTo(expected)
    }

    @Test
    fun `moviesSortedByRating throws Exception`() {
        val repository = RealTrendingRepository(ThrowsTrendingService())
        assertFailsWith<TimeoutException>() {
            runBlocking {
                repository.moviesSortedByRating(1)
            }
        }
    }

    @Test
    fun `showsSortedByRating throws Exception`() {
        val repository = RealTrendingRepository(ThrowsTrendingService())
        assertFailsWith<TimeoutException>() {
            runBlocking {
                repository.showsSortedByRating(1)
            }
        }
    }
}

private class ThrowsTrendingService : TrendingService {

    override suspend fun trendingMovies(page: Int): TrendingResults<TrendingMovie> {
        throw TimeoutException()
    }

    override suspend fun trendingShows(page: Int): TrendingResults<TrendingShow> {
        throw TimeoutException()
    }
}


private class FakeTrendingService : TrendingService {
    override suspend fun trendingMovies(page: Int): TrendingResults<TrendingMovie> {
        return TrendingResults(
            page = 1,
            totalPages = 10,
            totalResults = 100,
            results = listOf(
                TrendingMovie(
                    id = 1L,
                    overview = "Batman",
                    posterPath = "/Batman",
                    title = "Batman",
                    voteAverage = 90f,
                ),
                TrendingMovie(
                    id = 2L,
                    overview = "DoctorStrange",
                    posterPath = "/DoctorStrange",
                    title = "DoctorStrange",
                    voteAverage = 100f,
                )
            )
        )
    }

    override suspend fun trendingShows(page: Int): TrendingResults<TrendingShow> {
        return TrendingResults(
            page = 1,
            totalPages = 10,
            totalResults = 100,
            results = listOf(
                TrendingShow(
                    id = 1L,
                    name = "DragonBall",
                    overview = "DragonBall",
                    posterPath = "/DragonBall",
                    voteAverage = 90f,
                ),
                TrendingShow(
                    id = 2L,
                    name = "OnePiece",
                    overview = "OnePiece",
                    posterPath = "/OnePiece",
                    voteAverage = 100f,
                )
            )
        )
    }

}