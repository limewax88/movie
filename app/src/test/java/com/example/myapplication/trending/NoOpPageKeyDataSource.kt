package com.example.myapplication.trending

import androidx.paging.PagingSource
import androidx.paging.PagingState

object NoOpPageKeyDataSource : PagingSource<Int, TrendingItem>() {
    override fun getRefreshKey(state: PagingState<Int, TrendingItem>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TrendingItem> {
        return LoadResult.Invalid()
    }
}