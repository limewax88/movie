package com.example.myapplication.trending

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.myapplication.trending.paging.PagingResult
import com.example.myapplication.trending.paging.TrendingLoadingStatus
import com.example.myapplication.trending.paging.TrendingPagerFactory
import kotlinx.coroutines.channels.Channel

class FakeTrendingPagerFactory : TrendingPagerFactory {

    override val loadingChannel: Channel<TrendingLoadingStatus> = Channel<TrendingLoadingStatus>(capacity = Channel.CONFLATED)

    override fun create(loadData: suspend (Int) -> PagingResult): Pager<Int, TrendingItem> {
        return Pager(
            PagingConfig(pageSize = 1)
        ) {
            NoOpPageKeyDataSource
        }
    }
}