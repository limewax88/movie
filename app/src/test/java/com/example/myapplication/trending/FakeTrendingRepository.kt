package com.example.myapplication.trending

import com.example.myapplication.TestChannel
import com.example.myapplication.trending.paging.PagingResult

class FakeTrendingRepository: TrendingRepository {

    val showsResponse = TestChannel<PagingResult>()
    override suspend fun showsSortedByRating(page: Int): PagingResult {
        return showsResponse.awaitValue()
    }

    val moviesResponse = TestChannel<PagingResult>()
    override suspend fun moviesSortedByRating(page: Int): PagingResult {
        return moviesResponse.awaitValue()
    }
}