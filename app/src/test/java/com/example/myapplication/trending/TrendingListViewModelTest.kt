package com.example.myapplication.trending

import app.cash.molecule.testing.testMolecule
import com.example.myapplication.trending.TrendingListEvents.TryAgain
import com.example.myapplication.trending.movies.TrendingMoviesViewModel
import com.example.myapplication.trending.paging.TrendingLoadingStatus.InitialLoadingStatus
import com.example.myapplication.trending.paging.TrendingLoadingStatus.PaginatedLoadingStatus.*
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.test.runTest
import org.junit.Test

class TrendingListViewModelTest {

    private val fakeTrendingPagerFactory = FakeTrendingPagerFactory()

    @Test
    fun `emits initial ui model`() = runTest {
        val viewModel = createViewModel()
        val events = MutableSharedFlow<TrendingListEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
            with(awaitItem()) {
                assertThat(this.initialLoadingStatus).isEqualTo(InitialLoadingStatus.Loading)
                assertThat(this.paginatedLoadingStatus).isEqualTo(Idle)
            }
        }
    }

    @Test
    fun `emits ui model with initialLoadingStatus set to failed if request fails`() = runTest {
        val viewModel = createViewModel()
        val events = MutableSharedFlow<TrendingListEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
            awaitItem()
            val message = "error"
            fakeTrendingPagerFactory.loadingChannel.send(InitialLoadingStatus.Failure(message))
            with(awaitItem()) {
                assertThat(initialLoadingStatus).isEqualTo(
                    InitialLoadingStatus.Failure(message)
                )
                assertThat(paginatedLoadingStatus).isEqualTo(Idle)
            }
        }
    }

    @Test
    fun `emits pagination loading states`() = runTest {
        val viewModel = createViewModel()
        val events = MutableSharedFlow<TrendingListEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
            awaitItem()
            fakeTrendingPagerFactory.loadingChannel.send(Loading)
            with(awaitItem()) {
                assertThat(initialLoadingStatus).isEqualTo(InitialLoadingStatus.Loading)
                assertThat(paginatedLoadingStatus).isEqualTo(Loading)
            }
            fakeTrendingPagerFactory.loadingChannel.send(Idle)
            with(awaitItem()) {
                assertThat(initialLoadingStatus).isEqualTo(InitialLoadingStatus.Loading)
                assertThat(paginatedLoadingStatus).isEqualTo(Idle)
            }
        }
    }

    @Test
    fun `emits a new paged for event TryAgain`() = runTest {
        val viewModel = createViewModel()
        val events = MutableSharedFlow<TrendingListEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
            val initialData = awaitItem().list
            events.emit(TryAgain)
            assertThat(awaitItem().list).isNotSameInstanceAs(initialData)
        }
    }

    private fun createViewModel(): TrendingMoviesViewModel = TrendingMoviesViewModel(
        trendingRepository = FakeTrendingRepository(),
        moleculeMain = Dispatchers.Unconfined,
        trendingPagerFactory = fakeTrendingPagerFactory
    )
}