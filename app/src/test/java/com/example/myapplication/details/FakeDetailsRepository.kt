package com.example.myapplication.details

import com.example.myapplication.TestChannel
import com.example.myapplication.detail.DetailRepository
import com.example.myapplication.detail.DetailsItem

class FakeDetailsRepository(
    private val throws: Boolean = false
) : DetailRepository {

    val moviesResponse = TestChannel<DetailsItem>()
    val movieException = TestChannel<Exception>()
    override suspend fun movieById(id: Long): DetailsItem {
        return when (throws){
            true -> throw movieException.awaitValue()
            false -> moviesResponse.awaitValue()
        }
    }

    val showsResponse = TestChannel<DetailsItem>()
    val showException = TestChannel<Exception>()
    override suspend fun showById(id: Long): DetailsItem {
        return when (throws){
            true -> throw showException.awaitValue()
            false -> showsResponse.awaitValue()
        }
    }
}