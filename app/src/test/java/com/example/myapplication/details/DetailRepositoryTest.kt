package com.example.myapplication.details

import com.example.myapplication.ImageSizes
import com.example.myapplication.detail.DetailService
import com.example.myapplication.detail.DetailsItem
import com.example.myapplication.detail.RealDetailRepository
import com.example.myapplication.detail.movie.MovieDetailsResponse
import com.example.myapplication.detail.shows.TvDetailsResponse
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.util.concurrent.TimeoutException
import kotlin.test.assertFailsWith

class DetailRepositoryTest {

    @Test
    fun `get movieById returns DetailItem`() = runBlocking {
        val repository = RealDetailRepository(FakeDetailService())
        val expected = DetailsItem(
            imageUrl = "${ImageSizes.baseImageUrl}w1280/backdrop_path",
            title = "original_title",
            description = "overview",
            releaseDate = "release_date",
            runtime = 1L,
            rating = 1f
        )
        assertThat(repository.movieById(1L)).isEqualTo(expected)
    }

    @Test
    fun `getMovieById throws Exception`() {
        val repository = RealDetailRepository(ThrowsDetailsService())
        assertFailsWith<TimeoutException>() {
            runBlocking {
                repository.movieById(1L)
            }
        }
    }

    @Test
    fun `getShowById throws Exception`() {
        val repository = RealDetailRepository(ThrowsDetailsService())
        assertFailsWith<TimeoutException>() {
            runBlocking {
                repository.showById(1L)
            }
        }
    }

    @Test
    fun `get showById returns DetailItem`() = runBlocking {
        val repository = RealDetailRepository(FakeDetailService())
        val expected = DetailsItem(
            imageUrl = "${ImageSizes.baseImageUrl}w1280/backdrop_path",
            title = "original_name",
            description = "overview",
            releaseDate = "first_air_date",
            runtime = 1L,
            rating = 1f
        )
        assertThat(repository.showById(1L)).isEqualTo(expected)
    }
}

private class ThrowsDetailsService : DetailService {
    override suspend fun getMovieById(id: Long): MovieDetailsResponse {
        throw TimeoutException()
    }

    override suspend fun geTvById(id: Long): TvDetailsResponse {
        throw TimeoutException()
    }
}

private class FakeDetailService : DetailService {
    override suspend fun getMovieById(id: Long): MovieDetailsResponse {
        return MovieDetailsResponse(
            id = 1L,
            poster_path = "/poster_path",
            backdrop_path = "/backdrop_path",
            original_title = "original_title",
            overview = "overview",
            popularity = 1f,
            release_date = "release_date",
            revenue = 1L,
            runtime = 1L,
            title = "title",
            tagline = "tagline",
            vote_average = 1f,
            vote_count = 1L,
        )
    }

    override suspend fun geTvById(id: Long): TvDetailsResponse {
        return TvDetailsResponse(
            id = 1L,
            backdrop_path = "/backdrop_path",
            episode_run_time = listOf(1, 1, 1),
            first_air_date = "first_air_date",
            original_name = "original_name",
            overview = "overview",
            popularity = 1f,
            poster_path = "/poster_path",
            tagline = "tagline",
            vote_average = 1f,
            vote_count = 1L,
        )
    }

}