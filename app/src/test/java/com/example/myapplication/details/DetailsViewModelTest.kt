package com.example.myapplication.details

import androidx.lifecycle.SavedStateHandle
import app.cash.molecule.testing.testMolecule
import com.example.myapplication.common.UiState
import com.example.myapplication.detail.DetailsEvents
import com.example.myapplication.detail.DetailsItem
import com.example.myapplication.detail.DetailsViewModel
import com.example.myapplication.detail.movie.MovieDetailsViewModel
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.test.runTest
import org.junit.Test

class DetailsViewModelTest {

    @Test
    fun `emits initial ui model`() = runTest {
        val viewModel = createViewModel(FakeDetailsRepository())
        val events = MutableSharedFlow<DetailsEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
        }
    }

    @Test
    fun `emits successful ui model`() = runTest {
        val fakeDetailsRepository = FakeDetailsRepository()
        val viewModel = createViewModel(fakeDetailsRepository)
        val events = MutableSharedFlow<DetailsEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
            assertThat(awaitItem()).isEqualTo(UiState.Uninitialized)
            val item = DetailsItem(
                imageUrl = "",
                title = "",
                description = "",
                releaseDate = "",
                runtime = 100,
                rating = 100.23f,
            )
            events.emit(DetailsEvents.LoadDetails)
            assertThat(awaitItem()).isEqualTo(UiState.Loading)
            fakeDetailsRepository.moviesResponse.send(item)
            assertThat(awaitItem()).isEqualTo(UiState.Success(item))
        }
    }

    @Test
    fun `emits Error ui model`() = runTest {
        val fakeDetailsRepository = FakeDetailsRepository(throws = true)
        val viewModel = createViewModel(fakeDetailsRepository)
        val events = MutableSharedFlow<DetailsEvents>()
        testMolecule(body = {
            viewModel.models(events = events)
        }) {
            assertThat(awaitItem()).isEqualTo(UiState.Uninitialized)
            events.emit(DetailsEvents.LoadDetails)
            assertThat(awaitItem()).isEqualTo(UiState.Loading)
            fakeDetailsRepository.movieException.send(Exception())
            assertThat(awaitItem()).isEqualTo(UiState.Error)
        }
    }

    private fun createViewModel(
        fakeDetailsRepository: FakeDetailsRepository,
        savedStateHandle: SavedStateHandle = SavedStateHandle()
    ): DetailsViewModel = MovieDetailsViewModel(
        detailRepository = fakeDetailsRepository,
        moleculeMain = Dispatchers.Unconfined,
        savedStateHandle = savedStateHandle.apply {
            set("id", 0L)
        }
    )
}