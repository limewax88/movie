package com.example.myapplication

import app.cash.molecule.testing.MoleculeTurbine
import app.cash.molecule.testing.testMolecule
import com.example.myapplication.common.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalCoroutinesApi::class)
class TestChannel<T> {

    private val channel = Channel<T>(capacity = UNLIMITED)

    fun send(value: T) {
        channel.trySend(value)
    }

    val isEmpty: Boolean get() {return channel.isEmpty}

    fun asChannel(): Channel<T> = channel

    fun assertEmpty() {
        check(isEmpty) {
            "Unconsumed events"
        }
    }

    suspend fun awaitValue() = try {
        withTimeout(1000L) {
            channel.receive()
        }
    } catch (e: TimeoutCancellationException) {
        error("no value produced in 100ms")
    }
}

suspend fun <UiModel : Any, UiEvent : Any>BaseViewModel<UiModel, UiEvent>.test(
    eventChannel: TestChannel<UiEvent>,
    validate: suspend MoleculeTurbine<UiModel>.() -> Unit,
) {
    coroutineScope {
        val events = MutableSharedFlow<UiEvent>()
        val job = launch { eventChannel.asChannel().consumeAsFlow().collect(events::emit) }

        job.cancel()
    }
}