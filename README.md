# Entertainment  App

Simple entertainment application which loads movies and tv shows which connects to a REST API
from [TMDb](https://developers.themoviedb.org/3/getting-started/introduction) using the latest in Android technology.

## Development

* Built using [Android Studio](https://developer.android.com/studio)
* Entirely written in [Kotlin](https://kotlinlang.org/) 
* Uses [Molecule](https://github.com/cashapp/molecule)
* Uses [Kotlin Corouintes](https://kotlinlang.org/docs/coroutines-overview.html)
* Uses [Jetpack Compose](https://developer.android.com/jetpack/compose) for the UI
* Uses [Retrofit](https://developer.android.com/studio) for making network calls
* Uses [Dagger-Hilt](https://dagger.dev/hilt) for dependency injection
* Uses [Coil](https://coil-kt.github.io/coil/) for image loading
* Uses [MVI](https://www.raywenderlich.com/817602-mvi-architecture-for-android-tutorial-getting-started)

## Architecture

The app follows a Model-View-Intent design pattern

![flowchart](art/flowchart.png)

## Unit Testing

Most classes outside of the UI portions of the application were unit tested using the following dependencies

* [Molecule-Testing](https://github.com/cashapp/molecule#testing) to test the collection of Molecule Flow
* [Kotlin-Test](https://kotlinlang.org/docs/jvm-test-using-junit.html#run-a-test) for asserting the results


## Setup

First off, you require the latest [Android Studio](https://developer.android.com/studio) to be able to build the app.

### API keys

You need to supply an API Key. The app uses [TMDb](https://developers.themoviedb.org/3/getting-started/introduction)

When you obtain the keys, you can provide them to the app by putting the following in the
`gradle.properties` file in your user home:

```
# Get this from TMDb
TMDB_API_KEY=<insert>
```

On Linux/Mac that file is typically found at `~/.gradle/gradle.properties` or in the project directory `OpenMovie/gradle.properties`

## Screenshots

![TvList](art/tv_list.png)
![TvDetails](art/tv_details.png)
![MovieList](art/movie_list.png)
![MovieDetails](art/movie_details.png)




